Router.onBeforeAction(function(){
	cleanAlerts();
});

Router.configure(function(){
	loadingTemplate: 'loadingTemplate';
});

Router.map(function(){
	this.route('homeSubpage', {
		path:'/',
		layoutTemplate: 'basic',
	});

	this.route('signIn', {
		path:'/login', 
		layoutTemplate: 'basic'
	});

	this.route('signUp', {
		path: '/register', 
		layoutTemplate: 'basic'
	});

	this.route('forgotPassword', {
		path: '/password_reset', 
		layoutTemplate: 'basic'
	});

	this.route('adminPanel', {
		path:'/panel', 
		layoutTemplate:'panel'
	});

	this.route('manageUsers', {
		path: '/panel/users', 
		layoutTemplate: 'panel'
	});

	this.route('manageSites', {
		path: '/panel/sites',
		layoutTemplate: 'panel'
	});

	this.route('manageComments', {
		path: '/panel/comments',
		layoutTemplate: 'panel'
	});

	this.route('manageFiles', {
		path: '/panel/manageFiles',
		layoutTemplate: 'panel'
	});

	this.route('newSite', {
		path: '/panel/sites/new',
		layoutTemplate: 'panel'
	});

	this.route('website', {
		path: '/:friendlyName',
		layoutTemplate: 'basic',
		data: function(){ return Sites.findOne({friendlyName: this.params.friendlyName}); }
	});

	this.route('editSite', {
		path: '/panel/sites/:_id/edit',
		layoutTemplate: 'panel',
		data: function(){ return Sites.findOne(this.params._id); }
	});

	this.route('manageArticles', {
		path: '/panel/articles',
		layoutTemplate: 'panel'
	});

	this.route('newArticle', {
		path: '/panel/articles/new',
		layoutTemplate: 'panel'
	});

	this.route('editArticle', {
		path: '/panel/articles/:_id/edit',
		layoutTemplate: 'panel',
		data: function(){ return Articles.findOne(this.params._id); }
	});

	this.route('showArticle', {
		path: '/:site/:friendlyTitle',
		layoutTemplate: 'artLayout',
		data: function(){
			return Articles.findOne({friendlyTitle: this.params.friendlyTitle});
		}
	});
});

// Router.onBeforeAction(function(){
// 	if((!Meteor.user())||(Meteor.user().role !== 'superAdmin')){
// 		Router.go('signIn');
// 		throwAlert('Nie masz uprawnień do oglądania tej strony', 'notification');
// 	}
// },{except: ['homeSubpage', 'signIn', 'signUp', 'forgotPassword', 'website']});