Alerts = new Meteor.Collection(null);
Roles = new Meteor.Collection('roles');
Sites = new Meteor.Collection('sites');
Articles = new Meteor.Collection('articles');
Comments = new Meteor.Collection('comments');
Settings = new Meteor.Collection('settings');
Media = new FS.Collection('media', {
    stores: [new FS.Store.GridFS("media")]
});

//template helpers

Template.alertsTemplate.helpers({
    alerts: function(){
        return Alerts.find();
    }
});
Template.manageSites.helpers({
    sitesList: function(){
        return Sites.find({}, {sort: {order: 1}});
    }
});

Template.artPageContent.helpers({
    siteArticles: function(){
        return Articles.find({site: this.friendlyName, status: "published"});
    }
});

Template.homeSubpage.helpers({
    homePage: function(){
        return Sites.findOne({order: 1});
    },
});
Template.manageArticles.helpers({
    artsList: function(){
        return Articles.find({});
    }
});

Template.alertsTemplate.events({
    'click .closeButton': function(e, t){
        e.preventDefault();
        $(e.currentTarget).closest('.alert').fadeOut(300, function(){
            $(e.currentTarget).closest('.alert').remove();
        });
    },
    'click #alerts .alert': function(e, t){
        e.preventDefault();
        $(e.currentTarget).fadeOut(300, function(){
            $(e.currentTarget).remove();
        });
    }
});

Template.mainWebpageMenu.helpers({
    publishedWebsites: function(){
        return Sites.find({status: 'published'}, {sort: {order: 1}});
    }
});

Template.alert.rendered = function(){
    var alert = this.data;
    Meteor.defer(function(){
        Alerts.update(alert._id, {$set: {seen:true}});
    });
}

if(Accounts._resetPasswordToken){
    Session.set('resetPassword', Accounts._resetPasswordToken);
}
Template.basic.helpers({
    showForgotPassword:function(){
        return Session.get('showForgotPassword');
    },
    resetPassword:function(){
        return Session.get('resetPassword');
    },
    // webSettings: function(){
    //     return Settings.findOne({});
    // }
});

Template.newArticle.helpers({
    articleSites: function(){
        return Sites.find({type: 'artPage'});
    }
});

Template.manageComments.helpers({
    commentList: function(){
        return Comments.find({});
    },
    commentArticle: function(){
        return Articles.findOne(this.article).title;
    }
});

Template.artLayout.helpers({
    articleComments: function(){
        return Comments.find({article: this._id, visible: true}, {sort: {timestamp: -1}});
    }
});

// Template.manageFiles.helpers({
//     webSettings: function(){
//         return Settings.findOne({});
//     }
// });


//  rendering helpers

Template.contactPageContent.rendered = function(){
    runMap(this.data.mapX, this.data.mapY);
}

Template.newArticle.rendered = function(){
    var editor = new MediumEditor('.contentInput', {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'pre', 'image']
    });
}

Template.editArticle.rendered = function(){
    var editor = new MediumEditor('.contentInput', {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'pre', 'image']
    });
}

// UI & Spacebars/Handlebars helpers

UI.registerHelper('isEq', function(v1, v2){
    // console.log('isEq? '+v1 + ' '+v2);
    if(v1 === v2){
        return true;
    }else{
        return false;
    }
});

UI.registerHelper('shortIt', function(stringToShorten, maxCharsAmount){
  if(stringToShorten.length > maxCharsAmount){
    return stringToShorten.substring(0, maxCharsAmount) + '...';
  }
  return stringToShorten;
});

UI.registerHelper('hasRole', function(v1){
    if(currentUser.role === v1){
        return true;
    }else{
        return false;
    }
});

//function helpers

throwAlert = function(message, type){
    cleanAlerts();
    Alerts.insert({message: message, seen: false, type: type});
}

cleanAlerts = function(){
    Alerts.remove({seen: true});
}

isCurrentUser = function(role){
    if(Meteor.user().role === role){
        return true;
    }
    return false;
}

runMap = function(siteX, siteY){
    var map = L.mapbox.map('mapBox', 'ofcapl.i59mi3a3', {zoomControl: false}).setView([siteX, siteY], 9);

    L.mapbox.markerLayer({
        type: 'Feature',
        geometry: {
            type: 'Point',
            coordinates: [siteY, siteX]
        },
        properties: {
            'marker-color': '#000',
            'marker-symbol': 'post',
            title: 'HQ',
            description: 'HQ'
        }
    }).addTo(map);
}

getSitesAmount = function(){
    // console.log('sites amount: '+Sites.find({}).count());
    return Sites.find({}).count();
}

getFriendlyUrl = function(urlString){
    urlString = urlString.toLowerCase();
    urlString = urlString.replace('ł','l').replace('ó', 'o').replace('ś', 's').replace('ź', 'z').replace('ż', 'z').replace('ą', 'a').replace('ć', 'c').replace(/[^a-z0-9\s]/gi, '').replace(/[_\s]/g, '-');
    return urlString;
}

//autorun helpers

Deps.autorun(function(){
    Meteor.subscribe("userData");
    Meteor.subscribe("usersList");
    Meteor.subscribe("roles");
    Meteor.subscribe("sites");
    Meteor.subscribe("articles");
    Meteor.subscribe("comments");
    Meteor.subscribe("settings");
    Meteor.subscribe("media");
    Meteor.subscribe("publishedWebsites");
});

