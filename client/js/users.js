Template.signUp.events({
	'submit #signUpForm':function(e, t){
		e.preventDefault();

		var signUpForm = $(e.currentTarget),
			email = trimInput(signUpForm.find('#signUpEmail').val().toLowerCase()),
			password = signUpForm.find('#signUpPassword').val(),
			passwordConfirm = signUpForm.find('#signUpPasswordConfirm').val(),
			nickname = signUpForm.find('#signUpNick').val();

		if(isNotEmpty(email)&&isNotEmpty(password)&&isEmail(email)&&areValidPasswords(password, passwordConfirm)&&isValidNick(nickname)){
			Accounts.createUser({email: email, password: password, profile:{nick: nickname}}, function(error){
				if(error){
					if(error.error === 403){
						throwAlert('Email jest już użyty w bazie danych', 'error');
					}else{
						throwAlert('Przepraszamy, coś poszło nie tak', 'error');
					}
				}else{
					throwAlert('Pomyślnie zarejestrowano', 'success');
					Router.go('/login');                    
				}
			});
		}
		return false;
	},
});


Template.signOut.events({
	'click #signOutButton':function(e, t){
		Meteor.logout(function(){
			throwAlert('Pomyślnie wylogowano', 'success');
		});
		return false;
	}
});


Template.signIn.events({
	'submit #signInForm':function(e, t){
		e.preventDefault();

		var signInForm = $(e.currentTarget),
			email = trimInput(signInForm.find('.email').val().toLowerCase()),
			password = signInForm.find('.password').val();
		if(isNotEmpty(email)&&isEmail(email)&&isNotEmpty(password)&&isValidPassword(password)&&userIsActive(email)){
			Meteor.loginWithPassword(email, password, function(alert){
				if(alert){
					throwAlert('Podane dane są nieprawidłowe', 'error');
				}else{
					throwAlert('Pomyślnie zalogowano', 'success');
					$('#signInButton, #signUpButton').removeClass('active');
				}
			});
		}
		return false;
	},

	'click #signInButton': function(e, t){
		e.preventDefault();
		Session.set('showForgotPassword', null);
		return false;
	},

	'click #signUpButton': function(e, t){
		Session.set('showForgotPassword', null);
		return false;
	}
});


Template.forgotPassword.events({
	'submit #forgotPasswordForm':function(e, t){
		e.preventDefault();

		var forgotPasswordForm = $(e.currentTarget),
			email = trimInput(forgotPasswordForm.find('#forgotPasswordEmail').val().toLowerCase());

		if(isNotEmpty(email)&&isEmail(email)){
			Accounts.forgotPassword({email: email}, function(alert){
				if(alert){
					if(alert.error === 403){
						throwAlert('Nie ma takie adresu email w bazie danych', 'error');
					}else{
						throwAlert('Przepraszamy, coś poszło nie tak', 'error');
					}
				}else{
					throwAlert('Wysłano email resetujący hasło. Sprawdź swoją skrzynkę', 'notification');
				}
			});
		}
		return false;
	}
});


Template.resetPasswordTemplate.events({
	'submit #resetPasswordForm':function(e, t){
		e.preventDefault();

		var resetPasswordForm = $(e.currentTarget),
			password = resetPasswordForm.find('#resetPasswordPassword').val(),
			passwordConfirm = resetPasswordForm.find('#resetPasswordPasswordConfirm').val();
		if(isNotEmpty(password)&&areValidPasswords(password, passwordConfirm)){
			Accounts.resetPassword(Session.get('resetPassword'), password, function(alert){
				if(alert){
					throwAlert('Przepraszamy, ale coś poszło nie tak', 'error');
				}else{
					Router.go('/login');
					throwAlert('Twoje hasło zostało zmienione', 'success');
					Session.set('resetPassword', null);
				}
			});
		}
		return false;
	}
});


Template.basic.helpers({
	isSuperAdmin: function(){
		if(Meteor.users.findOne(Meteor.userId).role === 'superAdmin'){
			return true;
		}
		return false;
	},
});