trimInput = function(value){
    return value.replace(/^\s*|\s*$/g, '');
};

isNotEmpty = function(value){
    if(value && value !== ''){
        return true;
    }
    throwAlert('Wypełnij wszystkie wymagane pola', 'error');
    return false;
};

isEmail = function(value){
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(filter.test(value)){
        return true;
    }
    throwAlert('Adres email jest błędny', 'error');
    return false;
};

isValidNick = function(nick){
    if(nick.length < 3){
        throwAlert('Twój nick powinien składać się z co najmniej 3 znaków', 'error');
        return false;
    }
    return true;
}

isValidPassword = function(password){
    if(password.length < 6){
        throwAlert('Twoje hasło powinno składać się z co najmniej 6 znaków', 'error');
        return false;
    }
    return true;
};

areValidPasswords = function(password, confirm){
    if(!isValidPassword(password)){
        return false;
    }
    if(password !== confirm){
        throwAlert('Podane przez Ciebie hasła nie są takie same', 'error');
        return false;
    }
    return true;
};

isSiteNameUnique = function(siteName, siteId){
    var sitesWithName = Sites.findOne({name: siteName});
    // console.log('sites: '+sitesWithName.name);
    // console.log(sitesWithName._id);
    if(sitesWithName&&sitesWithName._id){
        if(siteId !== undefined){
            if(sitesWithName._id !== siteId){
                throwAlert('Już istnieje strona o takiej nazwie. Wprowadź inną nazwę', 'error');
                return false;
            }else{
                return true;
            }
        }else{
            throwAlert('Już istnieje strona o takiej nazwie. Wprowadź inną nazwę', 'error');
            return false;
        }
    };
    return true;
}

isSiteNameValid = function(siteName){
    var bannedNames = ['login', 'register', 'panel', 'password_reset'];
    for(var i=0; i< bannedNames.length-1; i++){
        if(siteName.toLowerCase() === bannedNames[i]){
            throwAlert('Podana nazwa strony jest niedozwolona, wpisz inną nazwę', 'error');
            return false;
        }
    }
    return true;
}

checkMapFields = function(type, mapX, mapY){
    if(type === 'contactPage'){
        if((mapX !== null)&&(mapY !== null)){
            return true;
        }else{
            throwAlert('Wypełnij pola dotyczące lokalizacji na mapie.', 'error');
            return false;
        }
    }else{
        return true;
    }
}

isArticleTitleUnique = function(articleName, articleId){
    var articlesWithTitle = Articles.findOne({title: articleName});
    
    if(articlesWithTitle&&articlesWithTitle._id){
        if(articleId !== undefined){
            if(articlesWithTitle._id !== articleId){
                throwAlert('Już istnieje artykuł o takim tytule. Wprowadź inny tytuł', 'error');
                return false;
            }else{
                return true;
            }
        }else{
            throwAlert('Już istnieje artykuł o takim tytule. Wprowadź inny tytuł', 'error');
            return false;
        }
    };
    return true;
}

commentHasBody = function(commentBody){
    commentBody = commentBody.replace(' ', '');
    if((commentBody !== undefined)&&(commentBody.length > 2)){
        return true;
    }else{
        throwAlert('Treść komentarza jest za krótka', 'error');
        return false;
    }
}

userIsActive = function(email){
    var user = Meteor.users.findOne({emails:{$elemMatch:{address:email}}});
    console.log(user.isActive);
    if(user.isActive){
        return true;
    }else{
        throwAlert('Użytkownik nie jest aktywny.', 'error');
        return false;
    }
}