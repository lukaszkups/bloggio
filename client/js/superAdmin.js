Template.manageUsers.helpers({
	usersList: function(){
		return Meteor.users.find({});
	},

	roles: function(){
		return Roles.find();
	}
});

Template.manageUsers.events({
    'click .checkbox': function(e, t){
        e.preventDefault();
        var checked = $(e.currentTarget).is(':checked');

        if(isCurrentUser('superAdmin')){
            Meteor.users.update({_id:this._id}, {$set:{isActive: checked}}, function(alert){
                if(alert.error === 403){
                    throwAlert('Coś poszło nie tak', 'error');
                }else{
                    throwAlert('Dane zaktualizowane', 'success');
                }
            });
        }
        return false;
    },

    'change select': function(e, t){
        e.preventDefault();
        var newRole = $(e.currentTarget).val();

        if(isCurrentUser('superAdmin')){
            Meteor.users.update({_id:this._id}, {$set:{role: newRole}}, function(alert){
                if(alert){
                    throwAlert('Coś poszło nie tak', 'error');
                }else{
                    throwAlert('Dane zaktualizowane', 'success');
                }
            });
        }else{
            throwAlert('Nie masz uprawnień do wykonania tej czynności', 'error');
        }
        return false;
    }
});

Template.manageSites.events({
    'click .deleteSiteButton': function(e, t){
        e.preventDefault();
        if(isCurrentUser('superAdmin')||isCurrentUser('admin')){
            Sites.remove(this._id);
        }
    },
    'click .orderUp': function(e, t){
        e.preventDefault();
        if(this.order != 1){
            if(isCurrentUser('superAdmin')||isCurrentUser('admin')){
                var currentItemOrder = this.order;
                var previousItemOrder = this.order - 1;
                Sites.update(Sites.findOne({order: previousItemOrder})._id, {$set: {order: currentItemOrder}});
                Sites.update(this._id, {$set: {order: previousItemOrder}});
                throwAlert('Pomyślnie zmieniono kolejność wyświetlania stron', 'success');
            }else{
                throwAlert('Nie masz uprawnień do wykonania tej czynności', 'error');
            }
        }
    },
    'click .orderDown': function(e, t){
        e.preventDefault();
        if(this.order < Sites.find({}).count()){
            if(isCurrentUser('superAdmin')||isCurrentUser('admin')){
                var currentItemOrder = this.order;
                var nextItemOrder = this.order + 1;
                Sites.update(Sites.findOne({order: nextItemOrder})._id, {$set: {order: currentItemOrder}});
                Sites.update(this._id, {$set: {order: nextItemOrder}});
                throwAlert('Pomyślnie zmieniono kolejność wyświetlania stron', 'success');
            }else{
                throwAlert('Nie masz uprawnień do wykonania tej czynności', 'error');
            }
        }
    }
});

Template.editSite.rendered = function(){
    var editor = new MediumEditor('.contentInput', {
        buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'pre', 'image']
    });    
};

Template.editSite.events({
    'submit #editSiteForm': function(e, t){
        e.preventDefault();
        var editSiteForm = $(e.currentTarget),
            siteName = editSiteForm.find('#siteName').val(),
            siteType = editSiteForm.find('#siteType').val(),
            siteStatus = editSiteForm.find('#siteStatusSelectBox').find(':selected').val(),
            siteOwner = editSiteForm.find('#siteOwner').val(),
            siteContent = $('.contentInput').html(),
            siteX, siteY, siteObj;

        switch(siteType){
            case 'contactPage':
                siteX = editSiteForm.find('#mapX').val();
                siteY = editSiteForm.find('#mapY').val();
                break;
            default:
                siteX = null;
                siteY = null;
        }

        siteObj = {
            name: siteName,
            content: siteContent,
            status: siteStatus,
            mapX: siteX,
            mapY: siteY,
            owner: siteOwner,
            friendlyName: getFriendlyUrl(siteName)
        }

        if(siteName&&isSiteNameUnique(siteName, this._id)&&checkMapFields(siteType, siteX, siteY)){
            Sites.update(this._id, {$set: siteObj}, function(alert){
                if(alert){
                    throwAlert('Coś poszło nie tak', 'error');
                    return false;
                }else{
                    Router.go('manageSites');
                    throwAlert('Zapisano zmiany', 'success');
                    console.log('siteObj: ');
                    console.log(siteObj);
                }
            });
        }else{
            throwAlert('Wypełnij wszystkie wymagane pola', 'error');
        }
        return false;
    },
});

Template.manageArticles.events({
    'click .deleteArticleButton':function(e, t){
        e.preventDefault();
        if(isCurrentUser('superAdmin')||isCurrentUser('admin')){
            Articles.remove(this._id);
        }
    },
});

Template.manageComments.events({
    'click .hideCommentButton':function(e, t){
        e.preventDefault();
        if(isCurrentUser('superAdmin')||isCurrentUser('admin')||isCurrentUser('moderator')){
            Comments.update(this._id, {$set: {visible: false}}, function(alert){
                if(alert){
                    throwAlert('Coś poszło nie tak', 'error');
                    return false;
                }else{
                    throwAlert('Komentarz został ukryty na stronie', 'success');
                }
            });
        }else{
            throwAlert('Nie masz uprawnień do wykonania tej czynności', 'error');
        }
    },
    'click .showCommentButton':function(e, t){
        e.preventDefault();
        if(isCurrentUser('superAdmin')||isCurrentUser('admin')||isCurrentUser('moderator')){
            Comments.update(this._id, {$set: {visible: true}}, function(alert){
                if(alert){
                    throwAlert('Coś poszło nie tak', 'error');
                    return false;
                }else{
                    throwAlert('Komentarz jest widoczny na stronie', 'success');       
                }
            });
        }else{
            throwAlert('Nie masz uprawnień do wykonania tej czynności', 'error');
            return false;
        }
    }
});

Template.manageFiles.events({
    'change #fileUploader': function(e, t){
        var files = e.target.files;
        for(var i=0; i<files.length; i++){
            Media.insert(files[i], function(error, fileObj){
                console.log(fileObj);
            });
        }
    },
    'click .deleteFileButton': function(e, t){
        e.preventDefault();
        if(isCurrentUser('superAdmin')||isCurrentUser('admin')){
            Media.remove(this._id);
        }
    }
});

Template.manageFiles.helpers({
    mediaList: function(){
        return Media.find({});
    },
});