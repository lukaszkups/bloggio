Template.newSite.events({
	'change #siteTypeSelectBox': function(e, t){
		e.preventDefault();
		var siteType = $(e.currentTarget).val();
		console.log(siteType)
		$('#addtionalSiteFieldsContainer').empty();
		switch(siteType){
			case 'contactPage':
				$('#mapFields').empty().append('<div class="line"><div class="box50 box left"><input id="mapX" type="text" placeholder="Dane mapy X"></div><div class="box50 box right"><input id="mapY" type="text" placeholder="Dane mapy Y"></div></div>');
				$('#addtionalSiteFieldsContainer').append('<div class="contentInput contentText" data-placeholder="Podaj dane kontaktowe..."></div>');
				var editor = new MediumEditor('.contentInput', {
					buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'pre', 'image']
				});
				break;
			default:
				$('#mapFields').empty();
				$('#addtionalSiteFieldsContainer').empty().append('<div class="contentInput contentText" data-placeholder="Wprowadź treść strony..."></div>');

				var editor = new MediumEditor('.contentInput', {
					buttons: ['bold', 'italic', 'underline', 'anchor', 'header1', 'header2', 'quote', 'pre', 'image']
				});
		}
	},
	'submit #newSiteForm': function(e, t){
		e.preventDefault();
		var newSiteForm = $(e.currentTarget),
			siteName = newSiteForm.find('#siteName').val(),
			siteStatus = newSiteForm.find('#siteStatusSelectBox').find(':selected').val(),
			siteType = newSiteForm.find('#siteTypeSelectBox').find(':selected').val(),
			siteX = null,
			siteY = null,
			siteContent = $('.contentInput').html(),
			siteOrder = getSitesAmount() + 1,
			siteObj;

		switch(siteType){
			case 'contactPage':
				siteX = newSiteForm.find('#mapX').val();
				siteY = newSiteForm.find('#mapY').val();
				break;
		}
		siteContent = $('.contentInput').html();
		siteObj = {
			name: siteName,
			type: siteType,
			content: siteContent,
			mapX: siteX,
			mapY: siteY,
			order: siteOrder,		//because new site == last site in order
			owner: Meteor.user().nick,
			status: siteStatus,
			friendlyName: getFriendlyUrl(siteName)
		}
		console.log('siteObj:');
		console.log(siteObj);
		console.log('siteType: '+siteType+', siteX: '+siteX+', siteY: '+siteY+', checkMapFields: '+checkMapFields('bla', 12, 32));
		if(isSiteNameUnique(siteName)&&isSiteNameValid(siteName)&&siteName&&siteType&&siteStatus&&checkMapFields(siteType, siteX, siteY)){
			var siteId = Sites.insert(siteObj);
			if(siteId){
				Router.go('manageSites');
				throwAlert('Dodano stronę', 'success');
			}else{
				throwAlert('Wystąpił błąd', 'error');
			}
		}else{
			throwAlert('Wypełnij wszystkie wymagane pola', 'error');
		}
		return false;
	},

});