Template.newArticle.events({
	'submit #newArticleForm':function(e, t){
		e.preventDefault();
		var newArticleForm = $(e.currentTarget),
			articleTitle = newArticleForm.find('#articleTitle').val(),
			articleSite = newArticleForm.find('#siteTypeSelectBox').find(':selected').val(),
			articleStatus = newArticleForm.find('#articleStatusSelectBox').find(':selected').val(),
			articleComments = newArticleForm.find('#articleComments').val(),
			articleContent = $('.contentInput').html(),
			timestamp, articleDate, articleObj;

		articleDate = new Date();
		articleDate = articleDate.format("dd/mm/yyyy");

		articleObj = {
			title: articleTitle,
			date: articleDate,
			content: articleContent,
			author: Meteor.user().nick,
			comments: articleComments,
			site: articleSite,
			status: articleStatus,
			friendlyTitle: getFriendlyUrl(articleTitle),
			timestamp: new Date().format("yyyymmddhhmm")
		};

		if(articleTitle&&articleSite&&articleStatus&&isArticleTitleUnique(articleTitle)){
			var articleId = Articles.insert(articleObj);
			if(articleId){
				Router.go('manageArticles');
				throwAlert('Dodano artykuł', 'success');
			}else{
				throwAlert('Wystąpił błąd', 'error');
			}
		}else{
			throwAlert('Wypełnij wszystkie wymagane pola', 'error');
		}
		return false;
	},
});

Template.editArticle.events({
	'submit #editArticleForm':function(e, t){
		e.preventDefault();
		var editArticleForm = $(e.currentTarget),
			articleTitle = editArticleForm.find('#articleTitle').val(),
			articleStatus = editArticleForm.find('#articleStatusSelectBox').find(':selected').val(),
			articleComments = editArticleForm.find('#articleComments').val(),
			articleContent = $('.contentInput').html(),
			articleObj;

		articleObj = {
			title: articleTitle,
			content: articleContent,
			comments: articleComments,
			status: articleStatus,
			friendlyTitle: getFriendlyUrl(articleTitle),
		};
		// need to check if friendly URL is unique and if not then add timestamp
		if(articleTitle&&articleStatus&&isArticleTitleUnique(articleTitle, this._id)){
            Articles.update(this._id, {$set: articleObj}, function(alert){
            	if(alert){
            		throwAlert('Coś poszło nie tak', 'error');
            	}else{
            		Router.go('manageArticles');
            		throwAlert('Zapisano zmiany', 'success');
            	}
            });
		}else{
			throwAlert('Pola nie mogą być puste!', 'error');
		}
		return false;
	},
});