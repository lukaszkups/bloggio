Template.addComment.events({
	'submit #commentForm':function(e, t){
		e.preventDefault();
		var commentForm = $(e.currentTarget),
			commentContent = commentForm.find('#commentContent').val(),
			commentAuthor = Meteor.user().nick,
			commentVisibility = true,
			commentArticle = this._id,
			commentTimestamp = new Date().getTime(),
			commentDate, commentObj;

		console.log('template id: '+commentArticle);

		commentDate = new Date();
		commentDate = commentDate.format("dd/mm/yyyy");
		commentObj = {
			content: commentContent,
			author: commentAuthor,
			date: commentDate,
			visible: commentVisibility,
			article: commentArticle,
			timestamp: commentTimestamp
		}
		if(Meteor.user()&&commentHasBody(commentContent)){
			var commentId = Comments.insert(commentObj);
			if(commentId){
				throwAlert('Dodano komentarz!', 'success');
				commentForm.find('#commentContent').val('');	//clear the comment form content
			}
		}else{
			throwAlert('Przepraszamy, coś poszło nie tak', 'error');
		}
		return false;
	}
});