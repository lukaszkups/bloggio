Meteor.users.allow({
    update: function(userId, doc, fields, modifier){
        if((userId && (doc._id === userId))||isSuperAdmin(Meteor.users.findOne(userId).role)){
            return true;
        }
    }
});