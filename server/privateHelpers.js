//define collections
Roles = new Meteor.Collection('roles');
Sites = new Meteor.Collection('sites');
Articles = new Meteor.Collection('articles');
Comments = new Meteor.Collection('comments');
Settings = new Meteor.Collection('settings');
Media = new FS.Collection('media', {
	stores: [new FS.Store.GridFS("media")]
});


//publish collections
Meteor.publish("userData", function(){
	return Meteor.users.find({_id: this.userId}, {fields: {profile: 1, role: 1, isActive: 1, nick: 1}});
});

Meteor.publish("usersList", function(){
	return Meteor.users.find({}, {fields: {emails: 1, profile: 1, role: 1, isActive: 1, nick: 1}});
});

Meteor.publish('roles', function(){
	return Roles.find({}, {fields: {name: 1}});
});

Meteor.publish('sites', function(){
	return Sites.find({}, {fields: {name: 1, order: 1, owner: 1, type: 1, content: 1, mapX: 1, mapY: 1, status: 1}});
});

Meteor.publish('articles', function(){
	return Articles.find({}, {fields: {title: 1, date: 1, content: 1, author: 1, comments: 1, site: 1, status: 1}});
});

Meteor.publish('comments', function(){
	return Comments.find({}, {fields: {author: 1, content: 1, date: 1, visible: 1, article: 1, timestamp: 1}});
});

Meteor.publish('settings', function(){
	return Settings.findOne({}, {fields: {dropboxApi: 1, customCss: 1, customJs: 1}});
});

// Meteor.publish('media', function(){
// 	return Media.find({});
// });

Meteor.publish('publishedWebsites', function(){
	return Sites.find({status: 'published'}, {sort: {order: 1}});
});

//helper methods
Meteor.methods({
	'fileUpload': function(fileInfo, fileData){
		console.log("received file " + fileInfo.name + " data: " + fileData);
		FS.writeFile(fileInfo.name, fileData);
	}
});

//helper functions
registrationRole = function(){
	if(Meteor.users.find().count() === 0){
		role = 'superAdmin';
	}else{
		role = 'user'
	}
	return role;
}

isSuperAdmin = function(role){
	if(role === 'superAdmin'){
		return true;
	}
	return false;
}

Accounts.onCreateUser(function(options, user){
	user.role = registrationRole();
	user.isActive = isSuperAdmin(user.role);
	user.nick = options.profile.nick;

	if(options.profile){    //just in case user.profile override
		user.profile = options.profile;
	}
	return user;
});

//seed the data:
if(Roles.find().count() === 0){
    Roles.insert({name: 'superAdmin'});
    Roles.insert({name: 'admin'});
    Roles.insert({name: 'moderator'});
    Roles.insert({name: 'user'});
}